"""
@author: Joren Zandstra

Sets the metrics in the Configuration object, given a telegram from the smart meter.
"""
from __future__ import annotations

import datetime
import logging

from dsmr_parser.objects import Telegram
from prometheus_client import Gauge
from smart_meter_monitoring import Configuration


def set_metrics(config: Configuration, t: Telegram) -> None:
    """
    Reads the t contents and sets the configured metrics appropriately.

        Parameters
    ----------
    config : Configuration
        This package's Configuration object, with the metrics set.
    t: t
        A t from the smart meter.
    """
    logging.debug(f"[    ] {t=}")

    # set Gauge metrics
    _set_metric(
        config,
        "smartmeter_current_amperes",
        t.INSTANTANEOUS_CURRENT_L1.value,
        {"phase": "L1"},
    )
    _set_metric(
        config,
        "smartmeter_current_amperes",
        t.INSTANTANEOUS_CURRENT_L2.value,
        {"phase": "L2"},
    )
    _set_metric(
        config,
        "smartmeter_current_amperes",
        t.INSTANTANEOUS_CURRENT_L3.value,
        {"phase": "L3"},
    )

    _set_metric(
        config,
        "smartmeter_gas_cubicmeters_total",
        t.HOURLY_GAS_METER_READING.value,
    )

    _set_metric(
        config,
        "smartmeter_long_power_failures_total",
        t.LONG_POWER_FAILURE_COUNT.value,
    )
    _set_metric(
        config,
        "smartmeter_power_failures_total",
        t.SHORT_POWER_FAILURE_COUNT.value,
    )

    _set_metric(
        config,
        "smartmeter_power_kilowatthours_total",
        t.ELECTRICITY_USED_TARIFF_1.value,
        {"rate": "low", "type": "in"},
    )
    _set_metric(
        config,
        "smartmeter_power_kilowatthours_total",
        t.ELECTRICITY_USED_TARIFF_2.value,
        {"rate": "high", "type": "in"},
    )
    _set_metric(
        config,
        "smartmeter_power_kilowatthours_total",
        t.ELECTRICITY_DELIVERED_TARIFF_1.value,
        {"rate": "low", "type": "out"},
    )
    _set_metric(
        config,
        "smartmeter_power_kilowatthours_total",
        t.ELECTRICITY_DELIVERED_TARIFF_2.value,
        {"rate": "high", "type": "out"},
    )

    _set_metric(
        config,
        "smartmeter_power_kilowatts",
        t.INSTANTANEOUS_ACTIVE_POWER_L1_POSITIVE.value,
        {"phase": "L1", "type": "in"},
    )
    _set_metric(
        config,
        "smartmeter_power_kilowatts",
        t.INSTANTANEOUS_ACTIVE_POWER_L2_POSITIVE.value,
        {"phase": "L2", "type": "in"},
    )
    _set_metric(
        config,
        "smartmeter_power_kilowatts",
        t.INSTANTANEOUS_ACTIVE_POWER_L3_POSITIVE.value,
        {"phase": "L3", "type": "in"},
    )
    _set_metric(
        config,
        "smartmeter_power_kilowatts",
        t.INSTANTANEOUS_ACTIVE_POWER_L1_NEGATIVE.value,
        {"phase": "L1", "type": "out"},
    )
    _set_metric(
        config,
        "smartmeter_power_kilowatts",
        t.INSTANTANEOUS_ACTIVE_POWER_L2_NEGATIVE.value,
        {"phase": "L2", "type": "out"},
    )
    _set_metric(
        config,
        "smartmeter_power_kilowatts",
        t.INSTANTANEOUS_ACTIVE_POWER_L3_NEGATIVE.value,
        {"phase": "L3", "type": "out"},
    )

    _set_metric(
        config,
        "smartmeter_power_kilowatts_total",
        t.CURRENT_ELECTRICITY_USAGE.value,
        {"type": "in"},
    )
    _set_metric(
        config,
        "smartmeter_power_kilowatts_total",
        t.CURRENT_ELECTRICITY_DELIVERY.value,
        {"type": "out"},
    )

    _set_metric(config, "smartmeter_power_tariff", t.ELECTRICITY_ACTIVE_TARIFF.value)

    timestamp_unix = (t.P1_MESSAGE_TIMESTAMP.value).timestamp()
    _set_metric(config, "smartmeter_reported_timestamp", timestamp_unix)

    _set_metric(
        config,
        "smartmeter_voltage_behavior_total",
        t.VOLTAGE_SAG_L1_COUNT.value,
        {"phase": "L1", "type": "sag"},
    )
    _set_metric(
        config,
        "smartmeter_voltage_behavior_total",
        t.VOLTAGE_SAG_L2_COUNT.value,
        {"phase": "L2", "type": "sag"},
    )
    _set_metric(
        config,
        "smartmeter_voltage_behavior_total",
        t.VOLTAGE_SAG_L3_COUNT.value,
        {"phase": "L3", "type": "sag"},
    )
    _set_metric(
        config,
        "smartmeter_voltage_behavior_total",
        t.VOLTAGE_SWELL_L1_COUNT.value,
        {"phase": "L1", "type": "swell"},
    )
    _set_metric(
        config,
        "smartmeter_voltage_behavior_total",
        t.VOLTAGE_SWELL_L2_COUNT.value,
        {"phase": "L2", "type": "swell"},
    )
    _set_metric(
        config,
        "smartmeter_voltage_behavior_total",
        t.VOLTAGE_SWELL_L3_COUNT.value,
        {"phase": "L3", "type": "swell"},
    )

    _set_metric(
        config,
        "smartmeter_voltage_volts",
        t.INSTANTANEOUS_VOLTAGE_L1.value,
        {"phase": "L1"},
    )
    _set_metric(
        config,
        "smartmeter_voltage_volts",
        t.INSTANTANEOUS_VOLTAGE_L2.value,
        {"phase": "L2"},
    )
    _set_metric(
        config,
        "smartmeter_voltage_volts",
        t.INSTANTANEOUS_VOLTAGE_L3.value,
        {"phase": "L3"},
    )

    # collect info metric content separately since we cannot incrementally set the labels
    info_metric_labels = {}

    device_type = str(t.DEVICE_TYPE.value)  # returns an int otherwise
    logging.debug(
        f"[    ] info metric | add {device_type=}",
    )
    info_metric_labels["device_type"] = device_type

    equipment_identifier_electricity = t.EQUIPMENT_IDENTIFIER.value
    equipment_identifier_electricity = _parse_smart_meter_octet_string(
        equipment_identifier_electricity,
    )
    logging.debug(
        f"[    ] info metric | add {equipment_identifier_electricity=}",
    )
    info_metric_labels[
        "equipment_identifier_electricity"
    ] = equipment_identifier_electricity

    equipment_identifier_gas = t.EQUIPMENT_IDENTIFIER_GAS.value
    equipment_identifier_gas = _parse_smart_meter_octet_string(equipment_identifier_gas)
    logging.debug(
        f"[    ] info metric | add {equipment_identifier_gas=}",
    )
    info_metric_labels["equipment_identifier_gas"] = equipment_identifier_gas

    p1_output_version = t.P1_MESSAGE_HEADER.value
    logging.debug(
        f"[    ] info metric | add {p1_output_version=}",
    )
    info_metric_labels["p1_output_version"] = p1_output_version

    config.info_metric.info(info_metric_labels)

    # log power failure events
    # returns a list of
    # [
    #  [ value: event_count, unit: None],
    #  [ value: OBIS reference, unit: None],
    #  and then zero or more pairs of
    #  [ value: timestamp, unit: None ]  # start of power failure event
    #  [ value: int, unit: s ]  # duration of power failure event
    #  ...
    # ]
    event_list: list[
        dict[int | datetime.datetime | str, str | None]
    ] = t.POWER_EVENT_FAILURE_LOG.value
    event_count = event_list[0]["value"]

    if event_count > 0:
        logging.info(f"smart meter reports {event_count} power failure events:")
        events = event_list[2:]
        events = [events[i : i + 2] for i in range(0, len(events), 2)]
        for index, (event_end, event_duration) in enumerate(events):
            end = event_end["value"].isoformat()
            start = (
                event_end["value"] - datetime.timedelta(seconds=event_duration["value"])
            ).isoformat()
            logging.info(
                f"power failure event {index+1}/{event_count}: {start=}, {end=}",
            )

    # log text messages
    text_message = t.TEXT_MESSAGE.value
    if text_message is not None:
        text_message = _parse_smart_meter_octet_string(text_message)
        logging.info(
            f"smart meter output contained a message: '{text_message}'",
        )


def _set_metric(
    config: Configuration,
    metric_name: str,
    value: float,
    metric_labels: dict[str, str] | None = None,
) -> None:
    metric: Gauge = config.metrics[metric_name]
    logging.debug(f"[    ] setting {metric=} with {metric_labels=} to {value=}")
    if metric_labels:
        metric.labels(**metric_labels).set(value)
    else:
        metric.set(value)


def _parse_smart_meter_octet_string(octet_string: str) -> str:
    """
    parse_smart_meter_octet_string parse a smart meter alphanumeric octet string.

    See the DSMR spec, 'Sn' is an n-length alphanumeric string.
    'Sn, tag 9' is an octet string, where the octets are hex-encoded.

    Parameters
    ----------
    octet_string : str
        string of hex-encoded octets as output by the smart meter

    Returns
    -------
    str
        the decoded message
    """
    logging.debug(f"[    ] {octet_string=}")
    hex_values: list[str] = [
        octet_string[i : i + 2] for i in range(0, len(octet_string), 2)
    ]
    octal_values: list[str] = [oct(int(el, 16)) for el in hex_values]
    octal_ints: list[int] = [int(el, 8) for el in octal_values]
    chars: list[str] = [chr(el) for el in octal_ints]
    message: str = "".join(chars)
    logging.debug(
        f"[    ] {octet_string=} {hex_values=} {octal_values=} {octal_ints=} {chars=}",
    )
    logging.debug(f"[ OK ] {octet_string=} {message=}")

    return message
