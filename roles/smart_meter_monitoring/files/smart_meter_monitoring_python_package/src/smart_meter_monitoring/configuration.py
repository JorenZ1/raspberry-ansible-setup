"""
@author: Joren Zandstra

Imports and sets a Configuration object from the program's configuration .toml file.

Also registers the relevant metrics with Prometheus.
"""
from __future__ import annotations

import typing as t

import tomli
from prometheus_client import CollectorRegistry
from prometheus_client import Gauge
from prometheus_client import Info


class Configuration:
    """
    Sets the program configuration.
    """

    def __init__(self, config_file_path: str) -> None:
        with open(config_file_path, "rb") as f:
            toml_data: dict[str, t.Any] = tomli.load(f)

        self.log_format: str = toml_data["general"]["log_format"]
        self.log_level: str = toml_data["general"]["log_level"]
        self.output_path: str = toml_data["general"]["output_path"]

        self.metrics_registry = CollectorRegistry()
        self.metrics: dict[str, Gauge] = {}
        self.info_metric: Info = Info(
            name="smartmeter",
            documentation="various informational labels",
            registry=self.metrics_registry,
        )
        self._register_metrics()

        print("running with the following config:")
        for k, v in self.__dict__.items():
            if k == "metrics":
                for metrics_key, metric in v.items():
                    print(f"{k:20s}: {metrics_key:40s}: {metric}")
            else:
                print(f"{k:20s}: {v}")

    def _register_metrics(self) -> None:
        """Register the Prometheus metrics in Python.

        This registers the relevant metrics.
        These are a combination of the smart meter outputs:
        some values in the telegram map to the same metric, only differing in labels.
        All metrics are initialized as Gauge, except for Info metric.
        """
        self.metrics["smartmeter_current_amperes"] = Gauge(
            name="smartmeter_current_amperes",
            documentation="Instantaneous current in A resolution, by phase (L1/L2/L3)",
            labelnames=["phase"],
            registry=self.metrics_registry,
        )
        self.metrics["smartmeter_gas_cubicmeters_total"] = Gauge(
            name="smartmeter_gas_cubicmeters_total",
            documentation="Last 5-minute value (temperature converted), gas delivered to client in m3, including decimal values and capture time",
            registry=self.metrics_registry,
        )
        self.metrics["smartmeter_long_power_failures_total"] = Gauge(
            name="smartmeter_long_power_failures_total",
            documentation="Number of long power failures in any phase",
            registry=self.metrics_registry,
        )
        self.metrics["smartmeter_power_failures_total"] = Gauge(
            name="smartmeter_power_failures_total",
            documentation="Number of power failures in any phase",
            registry=self.metrics_registry,
        )
        self.metrics["smartmeter_power_kilowatthours_total"] = Gauge(
            name="smartmeter_power_kilowatthours_total",
            documentation="Meter Reading electricity delivered by/to client in 0,001 kWh resolution, by rate (high/low), by type (in/out)",
            labelnames=["rate", "type"],
            registry=self.metrics_registry,
        )
        self.metrics["smartmeter_power_kilowatts"] = Gauge(
            name="smartmeter_power_kilowatts",
            documentation="Instantaneous active power in 1 W resolution, by phase (L1/L2/L3), by type (in/out)",
            labelnames=["phase", "type"],
            registry=self.metrics_registry,
        )
        self.metrics["smartmeter_power_kilowatts_total"] = Gauge(
            name="smartmeter_power_kilowatts_total",
            documentation="Actual electricity power delivered or received in 1 Watt resolution, by type (in/out)",
            labelnames=["type"],
            registry=self.metrics_registry,
        )
        self.metrics["smartmeter_power_tariff"] = Gauge(
            name="smartmeter_power_tariff",
            documentation="Tariff indicator electricity (1 = low tariff, 2 = high tariff). The tariff indicator can also be used to switch tariff dependent loads e.g boilers. This is the responsibility of the P1 user",  # noqa
            registry=self.metrics_registry,
        )
        self.metrics["smartmeter_reported_timestamp"] = Gauge(
            name="smartmeter_reported_timestamp",
            documentation="Date-time stamp of the P1 message",
            registry=self.metrics_registry,
        )
        self.metrics["smartmeter_voltage_behavior_total"] = Gauge(
            name="smartmeter_voltage_behavior_total",
            documentation="Voltage behavior, by phase (L1/L2/L3), by type (sag/swell)",
            labelnames=["phase", "type"],
            registry=self.metrics_registry,
        )
        self.metrics["smartmeter_voltage_volts"] = Gauge(
            name="smartmeter_voltage_volts",
            documentation="Instantaneous voltage in V resolution, by phase (L1/L2/L3)",
            labelnames=["phase"],
            registry=self.metrics_registry,
        )
