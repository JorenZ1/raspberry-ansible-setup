"""
@author: Joren Zandstra

Runs the smart meter monitoring package
"""
from __future__ import annotations

import datetime
import logging
import os
import time

from dsmr_parser import telegram_specifications  # type: ignore
from dsmr_parser.clients import SERIAL_SETTINGS_V5  # type: ignore
from dsmr_parser.clients import SerialReader  # type: ignore
from prometheus_client import write_to_textfile
from smart_meter_monitoring import Configuration
from smart_meter_monitoring import set_metrics


if __name__ == "__main__":
    # set config
    config_file_path: str = os.environ.get(
        "SMART_METER_MONITORING_CONFIG_PATH",
        "/tmp/smart_meter_monitoring_configuration.toml",
    )
    config = Configuration(config_file_path)

    # set up logging
    stream_handler = logging.StreamHandler()
    formatter = logging.Formatter(config.log_format)
    stream_handler.setFormatter(formatter)
    logging.basicConfig(
        level=config.log_level,
        format=config.log_format,
        handlers=[stream_handler],
    )
    logging.getLogger(__name__)

    # prepare reader
    serial_reader: SerialReader = SerialReader(
        device="/dev/ttyUSB0",
        serial_settings=SERIAL_SETTINGS_V5,
        telegram_specification=telegram_specifications.V5,
    )

    logging.info("starting main program loop")
    for t in serial_reader.read():
        start: datetime.datetime = datetime.datetime.now()

        logging.debug(f"{t=}")

        # update metrics
        set_metrics(config, t)

        # output metrics
        write_to_textfile(
            f"{config.output_path}/smart_meter_monitoring.prom",
            config.metrics_registry,
        )

        end: datetime.datetime = datetime.datetime.now()

        # cheat in a loop duration metric via the logs
        logging.info(f"run_duration={(end - start).microseconds / 1_000_000} seconds")

        time.sleep(2.5)
