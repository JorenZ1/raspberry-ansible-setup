# Smart meter monitoring

Homebrew package to provide Prometheus monitoring output from my home's "smart" meter.
It outputs relevant electricity values and gas values, after some calculations.

## Variables

The module reads the following environment variables:

| variable | default | effect |
| --- | --- | --- |
| SMART_METER_MONITORING_CONFIG_PATH | /tmp/smart_meter_monitoring.toml | the configuration file for program parameters |
