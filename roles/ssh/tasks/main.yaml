---
- name: Set system creation time epoch for comparisons
  block:
    - name: Set system creation time epoch | read file
      # YMMV; I think this file gets the datetime of the flash card writing
      ansible.builtin.stat:
        path: /boot/start.elf
      register: ssh_system_creation_timestamp_file

    - name: Set system creation time epoch | set epoch
      ansible.builtin.set_fact:
        ssh_system_creation_epoch: "{{ ssh_system_creation_timestamp_file.stat.mtime }}"

- name: Regenerate RSA and ED25519 keys
  block:
    - name: Regenerate RSA and ED25519 keys | read system key file
      ansible.builtin.stat:
        path: /etc/ssh/ssh_host_rsa_key
      register: ssh_host_rsa_key

    - name: Regenerate RSA and ED25519 keys | set system key creation epoch
      ansible.builtin.set_fact:
        ssh_host_rsa_key_creation_epoch: "{{ ssh_host_rsa_key.stat.mtime }}"

    - name: Regenerate RSA and ED25519 keys | regenerate RSA and ED25519 keys
      ansible.builtin.shell: |
        rm -f /etc/ssh/ssh_host_*
        ssh-keygen -t rsa -b 4096 -f /etc/ssh/ssh_host_rsa_key -N ""
        ssh-keygen -t ed25519 -f /etc/ssh/ssh_host_ed25519_key -N ""
      register: ssh_regenerate_keys
      changed_when: ssh_regenerate_keys.rc != 0
      notify:
        - Restart SSH
      when: ssh_system_creation_epoch|int >= ssh_host_rsa_key_creation_epoch|int

- name: Regenerate moduli
  block:
    - name: Regenerate moduli | read moduli file
      ansible.builtin.stat:
        path: /etc/ssh/moduli
      register: ssh_moduli

    - name: Regenerate moduli | set moduli creation epoch
      ansible.builtin.set_fact:
        ssh_moduli_creation_epoch: "{{ ssh_moduli.stat.mtime }}"

    - name: Regenerate moduli | regenerate moduli with only large values  # noqa: no-changed-when
      # Instead of removing small moduli, we just regenerate them all with large values
      # This task takes a long time, so we only run it only once, in async mode
      # On the next reboot, hopefully after this process finishes, all will be well
      ansible.builtin.shell: |
        ssh-keygen -M generate -O bits=4096 /etc/ssh/moduli-4096.candidates
        ssh-keygen -M screen -f /etc/ssh/moduli-4096.candidates /etc/ssh/moduli-4096
        mv /etc/ssh/moduli-4096 /etc/ssh/moduli
        rm /etc/ssh/moduli-4096.candidates
      async: 86400  # 1 day
      poll: 0  # we assume this runs to completion, in the background
      when: ssh_system_creation_epoch|int >= ssh_moduli_creation_epoch|int

- name: Place SSH client config
  ansible.builtin.template:
    src: ssh_config
    dest: /etc/ssh/
    owner: root
    group: root
    mode: "0644"
  notify:
    - Restart SSH

- name: Place SSH daemon config
  ansible.builtin.template:
    src: sshd_config
    dest: /etc/ssh/
    owner: root
    group: root
    mode: "0644"
  notify:
    - Restart SSH
