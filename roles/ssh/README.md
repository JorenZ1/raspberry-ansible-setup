# SSH

Sets up SSH.

The role is indebted to [SSHaudit](https://www.sshaudit.com/hardening_guides.html) for hardening recommendations.
Any deviations are my own.

## Note about regenerating the moduli

On fresh systems, this role will trigger a moduli regeneration.
This process will take a long time (hours on my pi; up to 24 hours I guess).
Since the pi might reboot due to other roles' handlers and requirements,
this role may need to be re-run by itself in order to trigger this moduli regeneration where it can run to completion without reboots.
