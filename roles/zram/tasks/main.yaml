---
- name: Check if zram is already running
  block:
    - name: Check if zram-config is already running | gather services info
      ansible.builtin.service_facts:

    - name: Check if zram-config is already running | set service facts
      ansible.builtin.set_fact:
        zram_zram_config_service_exists: "{{ 'zram-config.service' in hostvars[inventory_hostname].services.keys() }}"
        zram_zram_sync_service_exists: "{{ 'zsync.service' in hostvars[inventory_hostname].services.keys() }}"

- name: Install zram-config
  when: not zram_zram_config_service_exists
  block:
    - name: Install zram-config | install git
      ansible.builtin.apt:
        name: git
        state: present

    - name: Install zram-config | ensure directory
      ansible.builtin.file:
        path: "{{ zram_git_clone_dir }}"
        state: directory
        owner: root
        group: root
        mode: "0755"

    - name: Install zram-config | download zram-config
      ansible.builtin.git:
        repo: https://github.com/ecdye/zram-config
        dest: "{{ zram_git_clone_dir }}"
        version: "{{ zram_zram_config_version }}"

    - name: Install zram-config | install zram-config
      ansible.builtin.command: bash {{ zram_git_clone_dir }}/install.bash  # noqa: no-changed-when

- name: Update zram-config
  ansible.builtin.command: bash {{ zram_git_clone_dir }}/update.bash  # noqa: no-changed-when
  when: zram_zram_config_service_exists

- name: Install zram-config nightly sync to disk
  ansible.builtin.command: bash {{ zram_git_clone_dir }}/install.bash sync  # noqa: no-changed-when
  when: not zram_zram_sync_service_exists

- name: Enable zram-config service
  ansible.builtin.service:
    name: zram-config
    enabled: true

- name: Ensure custom zram dirs exist on disk
  ansible.builtin.include_tasks: ensure_custom_dirs.yaml
  loop: "{{ zram_custom_zram_dirs }}"
  loop_control:
    loop_var: zram_custom_zram_dir
  when: zram_custom_zram_dirs | count > 0

- name: Place zram config
  ansible.builtin.template:
    src: ztab.jinja2
    dest: /etc/ztab
    owner: root
    group: root
    mode: "0640"
  notify:
    - Restart zram-config
