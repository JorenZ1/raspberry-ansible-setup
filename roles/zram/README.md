# Set up zram

Installs [zram-config](https://github.com/ecdye/zram-config#example-configuration).
This makes it so the Raspberry has its swap files, log directory, and other customized folders in ram instead of on disk.
A nightly backup means you still have some disk writes, but a lot fewer than you'd otherwise have.

## Note: users and groups must exist!

This role configures optional zram directories, belonging to a user and a group.
The role assumes these users and groups exist.

It will error out if either user or group does not exist.
