# raspberry-ansible-setup

## Description
Ansible playbooks and related files for setting up my Raspberry Pi 
Ansible roles and related files to set up my Raspberry Pi for reading and parsing smart meter output (electricity produced/consumed, gas consumed):

* set up Raspberry Pi
* set up Python package for parsing the smart meter output
* set up Grafana cloud agent to send these metrics to my dashboards 

## Authors

* Joren Zandstra

## TODOs

* add proper GitLab pipeline for running the local pre-commit checks on the final code as well
* use a proper package for reading Smart Meter output instead of homebrew serial-port-reads
* break out the python package into a proper package on PyPI
